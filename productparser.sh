#!/bin/bash

#Test version format
echo $1

boolVersion=0
boolApis=0  

echo "--- Test version format ---"
version=$(cat $1 | yq .info.version)
echo "Version: $version"
Mayor=$(echo $version | cut -d\" -f2 | cut -d. -f1)
echo "Mayor: $Mayor"
minor=$(echo $version | cut -d. -f2)
echo "minor: $minor"
patch=$(echo $version | cut -d\" -f2 | cut -d. -f3)
echo "patch: $patch"

re='^[0-9]+$'
if ! [[ $Mayor =~ $re ]] ; then
   echo "error: Mayor version is not a number"
   boolVersion=1
   bool=1
fi
   
   
if ! [[ $minor =~ $re ]] ; then
   echo "error: minor version is not a number"
   boolVersion=1
   bool=1
fi
      
      
if ! [[ $patch =~ $re ]] ; then
   echo "error: patch version is not a number"
   boolVersion=1
   bool=1
fi

apis=$(cat $1 | yq .apis)

if [ -z "$apis" ]
then
	echo "No existen apis"
	boolApis=1
	bool=1
else
	echo "El producto tiene apis asociadas"
fi

if [ "$bool" == "1" ]
then
	echo "The product yaml has this errors: "
	if [ "$boolVersion" == "1" ]
	then
		echo "- The version format is not good"
	fi
	if [ "boolApis" == "1" ]
	then
		echo"- The product does not have apis"
	fi
else
	echo "The product yaml is ok"
fi


